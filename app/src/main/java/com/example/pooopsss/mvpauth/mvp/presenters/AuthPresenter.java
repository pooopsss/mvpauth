package com.example.pooopsss.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.pooopsss.mvpauth.mvp.models.AuthModel;
import com.example.pooopsss.mvpauth.mvp.views.IAuthView;
import com.example.pooopsss.mvpauth.ui.custom_views.AuthPanel;

/**
 * Created by pooopsss on 22.10.2016.
 */

public class AuthPresenter implements IAtuhPresenter {
    private static AuthPresenter ourInstance = new AuthPresenter();
    private AuthModel mAtuhModel;
    private IAuthView mAuthView;

    public  AuthPresenter(){
        mAtuhModel = new AuthModel();
    }

    public static AuthPresenter getInstance(){
        return  ourInstance;
    }

    @Override
    public void takeView(IAuthView authView) {
        mAuthView = authView;
    }

    @Override
    public void dropView() {

    }

    @Override
    public void initView() {
        if(getView() != null){
            if(checkUserAuth()){
                getView().hideLoginBtn();
            } else {
                getView().showLoginBtn();
            }
        }

    }

    @Nullable
    @Override
    public IAuthView getView() {
        return mAuthView;
    }

    @Override
    public void clickOnLogin() {
        if(getView() != null && getView().getAuthPanel() != null){
            if(getView().getAuthPanel().isIdle()){
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_SATE);
            } else {
                mAtuhModel.loginUser(getView().getAuthPanel().getUserEmail(), getView().getAuthPanel().getUserPassword());
                getView().showMessage("request users auth");
            }
        }
    }

    @Override
    public void clickOnFb() {
        if(getView() != null){
            getView().showMessage("Показать FB");
        }
    }

    @Override
    public void clickOnVk() {
        if(getView() != null){
            getView().showMessage("Показать VK");
        }
    }

    @Override
    public void clickOnTw() {
        if(getView() != null){
            getView().showMessage("Показать Twitter");
        }
    }

    @Override
    public void clickOnShowCatalog() {
        if(getView() != null){
            getView().showMessage("Показать каталог");
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mAtuhModel.isAuthUser();
    }


}
