package com.example.pooopsss.mvpauth.ui.custom_views;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;


import com.example.pooopsss.mvpauth.R;



public class AuthPanel extends LinearLayout {
    private static final String TAG = "AuthPanel";
    public static final int LOGIN_SATE = 0;
    public static final int IDLE_STATE = 1;
    private int mCustomState = 1;


    CardView mAuthCard;
    EditText mEmailEt;
    EditText mPasswordEt;
    Button mLoginBtn;
    Button mShowCatalogButton;
    TextInputLayout emailWrapper;
    TextInputLayout passwordWrapper;



    public AuthPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.state = mCustomState;
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCustomState(savedState.state);
    }

    public void setCustomState(int state) {
        mCustomState = state;
        showViewFromState();
    }

    private void showLoginState(){
        mAuthCard.setVisibility(VISIBLE);
        mShowCatalogButton.setVisibility(GONE);
    }

    private void showIdleState(){
        mAuthCard.setVisibility(GONE);
        mShowCatalogButton.setVisibility(VISIBLE);
    }

    private void showViewFromState(){
        if(mCustomState == LOGIN_SATE){
            showLoginState();
        } else {
            showIdleState();
        }
    }




    public String getUserEmail(){
        return String.valueOf(mEmailEt.getText());
    }
    public String getUserPassword(){
        return String.valueOf(mPasswordEt.getText());
    }

    public void setEtUserEmail(String email){
        mEmailEt.setText(email);
    }

    public void setEtUserPassword(String password){
        mPasswordEt.setText(password);
    }

    public boolean isIdle(){
        return mCustomState==IDLE_STATE;
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mLoginBtn = (Button) findViewById(R.id.login_btn);
        mAuthCard = (CardView) findViewById(R.id.auth_card);
        mEmailEt = (EditText) findViewById(R.id.login_email_et);
        mPasswordEt = (EditText) findViewById(R.id.login_passwrod_et);
        mShowCatalogButton = (Button) findViewById(R.id.show_catalog_btn);


        emailWrapper = (TextInputLayout) findViewById(R.id.login_email_wrap);
        passwordWrapper = (TextInputLayout) findViewById(R.id.login_passwrod_wrap);


        //region StartValidatorAction
        mEmailEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (android.util.Patterns.EMAIL_ADDRESS.matcher(s).matches() && s.length() > 0) {
                    emailWrapper.setError(null);
//                    if(mAuthCard.getVisibility() == View.VISIBLE)
//                        mLoginBtn.setEnabled(true);
                } else {
                    emailWrapper.setError("Введите правильно E-mail");
//                    if(mAuthCard.getVisibility() == View.VISIBLE)
//                        mLoginBtn.setEnabled(false);
                }


            }
        });
        //endregion

        showViewFromState();
    }

    static class SavedState extends BaseSavedState {
        private int state;

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }
            public SavedState[] newArray(int size){
                return new SavedState[size];
            }
        };

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public SavedState(Parcel in) {
            super(in);
            state = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(state);
        }
    }
}
