package com.example.pooopsss.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.pooopsss.mvpauth.mvp.views.IAuthView;


public interface IAtuhPresenter {

    void takeView(IAuthView authView);
    void dropView();
    void initView();

    @Nullable
    IAuthView getView();

    void clickOnLogin();
    void clickOnFb();
    void clickOnVk();
    void clickOnTw();

    void clickOnShowCatalog();

    boolean checkUserAuth();
}
