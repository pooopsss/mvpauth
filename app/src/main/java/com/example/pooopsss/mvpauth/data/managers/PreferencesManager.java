package com.example.pooopsss.mvpauth.data.managers;

import android.content.SharedPreferences;

import com.example.pooopsss.mvpauth.utils.MvpauthApplication;


public class PreferencesManager {
    private SharedPreferences mSharedPreferences;

    final static String FIELD_USER_EMAIL = "userEmail";
    final static String FIELD_USER_PASSWORD = "userPassw";

    public PreferencesManager() {
        this.mSharedPreferences = MvpauthApplication.getSharedPreferences();
    }

    public void SaveUserLoginPassword(String email, String passw){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(FIELD_USER_EMAIL, email);
        editor.putString(FIELD_USER_PASSWORD, passw);
        editor.apply();
    }

    public String getUserLogin(){
        return mSharedPreferences.getString(FIELD_USER_EMAIL, "");
    }
    public String getUserPassword(){
        return mSharedPreferences.getString(FIELD_USER_PASSWORD, "");
    }
}
