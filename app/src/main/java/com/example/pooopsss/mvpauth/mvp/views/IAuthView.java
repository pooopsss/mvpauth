package com.example.pooopsss.mvpauth.mvp.views;

import android.support.annotation.Nullable;

import com.example.pooopsss.mvpauth.mvp.presenters.AuthPresenter;
import com.example.pooopsss.mvpauth.ui.custom_views.AuthPanel;

/**
 * Created by pooopsss on 22.10.2016.
 */

public interface IAuthView {

    void showMessage(String message);
    void showError(Throwable e);

    void showLoad();
    void hideLoad();

    AuthPresenter getPresenter();

    @Nullable
    AuthPanel getAuthPanel();

    void showLoginBtn();
    void hideLoginBtn();

    //void textShowLoginCard();
}
