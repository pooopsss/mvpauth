package com.example.pooopsss.mvpauth.data.managers;

/**
 * Created by pooopsss on 22.10.2016.
 */

public class DataManager {
    private static DataManager INSTANCE = null;



    PreferencesManager preferencesManager;

    public DataManager() {
        preferencesManager = new PreferencesManager();
    }

    public static DataManager getInstance(){
        if(INSTANCE == null){
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public PreferencesManager getPreferencesManager() {
        return preferencesManager;
    }
}
