package com.example.pooopsss.mvpauth;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;

import com.example.pooopsss.mvpauth.data.managers.DataManager;
import com.example.pooopsss.mvpauth.mvp.presenters.AuthPresenter;
import com.example.pooopsss.mvpauth.mvp.views.IAuthView;
import com.example.pooopsss.mvpauth.ui.custom_views.AuthPanel;

public class RootActivity extends AppCompatActivity implements IAuthView , View.OnClickListener {
    AuthPresenter mPresenter = AuthPresenter.getInstance();

    CoordinatorLayout mCoordinatorLayout;
    Button mShowCatalogBtn;
    Button mLoginBtn;
    AuthPanel mAuthPanel;
    private DataManager mDataManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        mDataManager = DataManager.getInstance();

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_root);
        mShowCatalogBtn = (Button) findViewById(R.id.show_catalog_btn);
        mLoginBtn = (Button) findViewById(R.id.login_btn);
        mAuthPanel = (AuthPanel) findViewById(R.id.auth_wrapper);

        mAuthPanel.setEtUserEmail(mDataManager.getPreferencesManager().getUserLogin());
        mAuthPanel.setEtUserPassword(mDataManager.getPreferencesManager().getUserPassword());

        mPresenter.takeView(this);
        mPresenter.initView();

        mShowCatalogBtn.setOnClickListener(this);
        mLoginBtn.setOnClickListener(this);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if(!mAuthPanel.isIdle()){
            mAuthPanel.setCustomState(AuthPanel.IDLE_STATE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if(BuildConfig.DEBUG){
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(e.getMessage());
        }
    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    @Override
    public AuthPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(View.GONE);
    }

//    @Override
//    public void textShowLoginCard() {
//        mAuthCard.setVisibility(View.VISIBLE);
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.show_catalog_btn:
                mPresenter.clickOnShowCatalog();
                break;
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                break;
        }
    }
}
